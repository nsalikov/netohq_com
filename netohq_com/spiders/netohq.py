# -*- coding: utf-8 -*-
import time
import scrapy

from scrapy.shell import inspect_response
from scrapy.exceptions import CloseSpider, DontCloseSpider

from ..scrapy_selenium import SeleniumRequest
from selenium.webdriver.common.by import By
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class NetohqSpider(scrapy.Spider):
    name = 'netohq'
    allowed_domains = ['netohq.com']
    start_urls = ['https://www.netohq.com/support/s/knowledge#category=Support__c:Addons__c&categoryLabel=Addons']

    urls = []


    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super().from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_idle, signal=scrapy.signals.spider_idle)
        return spider


    def start_requests(self):
        for url in self.start_urls:
            yield SeleniumRequest(url=url, callback=self.parse)


    def parse(self, response):
        # inspect_response(response, self)
        driver = response.meta['driver']

        while True:
            try:
                timeout = 10
                links_xpath = '//*[@id="ServiceCommunityTemplate"]//h2[contains(@class, "slds-section__title")]/a'
                links = WebDriverWait(driver, timeout).until(EC.presence_of_all_elements_located((By.XPATH, links_xpath)))
            except Exception as e:
                self.logger.error('Unable to find addon URLs', exc_info=True)
            else:
                for link in links:
                    self.urls.append(link.get_attribute("href"))

            try:
                timeout = 10
                next_page_xpath = '//*[@id="ServiceCommunityTemplate"]//button[contains(@class, "pagination-button-active") and contains(text(), "Next")]'
                # next_page = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.XPATH, next_page_xpath)))
                next_page = WebDriverWait(driver, timeout).until(EC.element_to_be_clickable((By.XPATH, next_page_xpath)))

                driver.execute_script("return arguments[0].scrollIntoView(false);", next_page)
                driver.execute_script("window.scrollBy(0, 200);")
                next_page.click()
            except:
                self.logger.info('Breaking cycle', exc_info=True)
                break


    def spider_idle(self, spider):
        if self.urls:
            url = self.urls.pop(0)
            spider.crawler.engine.crawl(SeleniumRequest(url=url, callback=self.parse_addon), spider)
            raise DontCloseSpider


    def parse_addon(self, response):
        # inspect_response(response, self)
        driver = response.meta['driver']

        time.sleep(5)

        try:
            timeout = 10
            WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.XPATH, '//h1')))
        except Exception as e:
            self.logger.error('Unable to parse addon page', exc_info=True)
            return
        else:
            response = response.replace(body=str.encode(driver.page_source))

        d = {}

        d['url'] = response.url

        d['title'] = response.css('h1 ::text').extract_first()
        if d['title']:
            d['title'] = d['title'].strip()

        d['summary'] = response.css('.summary .article-summary ::text').extract_first()

        d['main_image'] = None
        d['image_urls'] = [response.urljoin(url) for url in response.css('.content img ::attr(src)').extract()]

        if d['image_urls']:
            d['main_image'] = d['image_urls'][0]

        d['content'] = response.css('.content').extract_first()

        return d
